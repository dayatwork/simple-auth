# Simple Auth

## Development

### Running App

```bash
# install dependencies
npm install

# run migration
npx prisma db push
# or
npx prisma migrate dev

# run in watch mode
npm run dev
```
