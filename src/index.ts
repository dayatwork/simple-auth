import { PrismaClient } from "@prisma/client";
import express, { Request, Response } from "express";
import cors from "cors";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import cookieParser from "cookie-parser";
import morgan from "morgan";
import { z } from "zod";

const prisma = new PrismaClient();
const app = express();

const ACCESS_TOKEN_SECRET = "secret";
const REFRESH_TOKEN_SECRET = "secret2";

app.use(express.json());
app.use(
  cors({
    credentials: true,
    origin: ["http://localhost:5173", "http://127.0.0.1:5173"],
  })
);
app.use(cookieParser());
app.use(morgan("dev"));

const signupSchema = z.object({
  email: z.string().email(),
  password: z.string().min(6),
  name: z.string().optional(),
  phone: z.string().optional(),
});

type SignUpDto = z.infer<typeof signupSchema>;

const loginSchema = z.object({
  email: z.string().email(),
  password: z.string().min(6),
});

type LoginDto = z.infer<typeof loginSchema>;

// Sign up
app.post("/auth/register", async (req: Request<{}, {}, SignUpDto>, res) => {
  const { email, password, name, phone } = req.body;

  try {
    // Validate body
    signupSchema.parse(req.body);

    // Check if user already exists
    const user = await prisma.user.findUnique({
      where: {
        email,
      },
    });

    if (user) {
      return res.status(400).json({
        error: "User already exists",
      });
    }

    // hash password
    const hashedPassword = await bcrypt.hash(password, 10);

    // create user
    const newUser = await prisma.user.create({
      data: {
        email,
        password: hashedPassword,
        name,
        phone,
      },
    });

    // create access token
    const accessToken = jwt.sign({ userId: newUser.id }, ACCESS_TOKEN_SECRET, {
      expiresIn: "15m",
    });

    // create refresh token
    const refreshToken = jwt.sign({ userId: newUser.id }, REFRESH_TOKEN_SECRET);

    // store refresh token in db
    await prisma.refreshToken.create({
      data: {
        token: refreshToken,
        userId: newUser.id,
      },
    });

    // send refresh and access token back
    res.cookie("refreshToken", refreshToken, {
      httpOnly: true,
      sameSite: "none",
      secure: true,
      expires: new Date(Date.now() + 7 * 24 * 60 * 60 * 1000),
    });

    return res.json({
      statusCode: 200,
      data: {
        accessToken,
        refreshToken,
      },
    });
  } catch (error: any) {
    return res.status(400).json({ message: error.message });
  }
});

// Login
app.post("/auth/login", async (req: Request<{}, {}, LoginDto>, res) => {
  const { email, password } = req.body;

  try {
    // Validate body
    loginSchema.parse(req.body);

    // Check if user exists
    const user = await prisma.user.findUnique({
      where: {
        email,
      },
    });

    if (!user) {
      return res.status(400).json({
        error: "User does not exist",
      });
    }

    // Check if password is correct
    const valid = await bcrypt.compare(password, user.password);

    if (!valid) {
      return res.status(400).json({
        error: "Incorrect password",
      });
    }

    // create access token
    const accessToken = jwt.sign({ userId: user.id }, ACCESS_TOKEN_SECRET, {
      expiresIn: "15m",
    });

    // create refresh token
    const refreshToken = jwt.sign({ userId: user.id }, REFRESH_TOKEN_SECRET);

    // store refresh token in db
    await prisma.refreshToken.create({
      data: {
        token: refreshToken,
        userId: user.id,
      },
    });

    // send refresh and access token back
    res.cookie("refreshToken", refreshToken, {
      httpOnly: true,
      sameSite: "none",
      secure: true,
      expires: new Date(Date.now() + 7 * 24 * 60 * 60 * 1000),
    });

    return res.json({
      statusCode: 200,
      data: {
        accessToken,
        refreshToken,
        // user,
      },
    });
  } catch (error: any) {
    return res.status(400).json({ message: error.message });
  }
});

// Logout
app.delete("/auth", async (req, res) => {
  const refreshToken = (req.cookies.refreshToken ||
    req.body.refreshToken) as string;

  // Check if refresh token exists
  if (!refreshToken) {
    return res.status(400).json({
      error: "Refresh token does not exist",
    });
  }

  // delete refresh token from db
  await prisma.refreshToken.delete({
    where: {
      token: refreshToken,
    },
  });

  // clear cookie
  res.clearCookie("refreshToken", {
    httpOnly: true,
    sameSite: "none",
    secure: true,
  });

  // send back success message
  return res.json({
    statusCode: 200,
    data: {
      message: "Logged out",
    },
  });
});

// Refresh token
app.put("/auth", async (req, res) => {
  const refreshToken = (req.cookies.refreshToken ||
    req.body.refreshToken) as string;

  // Check if refresh token exists
  if (!refreshToken) {
    return res.status(400).json({
      error: "Refresh token does not exist",
    });
  }

  // check if refresh token exists
  const token = await prisma.refreshToken.findUnique({
    where: {
      token: refreshToken,
    },
  });

  if (!token) {
    return res.status(401).json({
      error: "Invalid refresh token",
    });
  }

  // verify refresh token
  jwt.verify(
    refreshToken,
    REFRESH_TOKEN_SECRET,
    async (err: any, user: any) => {
      if (err) {
        return res.status(403).json({
          error: "Invalid refresh token",
        });
      }

      // create new access token
      const accessToken = jwt.sign(
        { userId: user.userId },
        ACCESS_TOKEN_SECRET,
        {
          expiresIn: "15m",
        }
      );

      // create new refresh token
      const newRefreshToken = jwt.sign(
        { userId: user.userId },
        REFRESH_TOKEN_SECRET
      );

      // upsert refresh token in db
      await prisma.refreshToken.upsert({
        where: { token: refreshToken },
        update: {
          token: newRefreshToken,
        },
        create: {
          token: newRefreshToken,
          userId: user.userId,
        },
      });

      // send back new refresh token
      res.cookie("refreshToken", newRefreshToken, {
        httpOnly: true,
        sameSite: "none",
        secure: true,
        expires: new Date(Date.now() + 7 * 24 * 60 * 60 * 1000),
      });

      return res.json({
        statusCode: 200,
        data: {
          accessToken,
          refreshToken: newRefreshToken,
        },
      });
    }
  );
});

// Get user
app.get("/auth", async (req, res) => {
  const accessToken = req.headers.authorization?.split(" ")[1];

  // check if access token exists
  if (!accessToken) {
    return res.status(401).json({
      error: "Access token not found",
    });
  }

  // verify access token
  jwt.verify(accessToken, ACCESS_TOKEN_SECRET, async (err: any, user: any) => {
    if (err) {
      return res.status(403).json({
        error: "Invalid access token",
      });
    }

    // get user
    const userFromDb = await prisma.user.findUnique({
      where: {
        id: user.userId,
      },
    });

    return res.json({
      statusCode: 200,
      data: {
        ...userFromDb,
      },
    });
  });
});

app.listen(4000, () => {
  console.log("Server started on port 3000");
});
